/* global site_url: true */
$(function(){
	
	var mobileview = window.matchMedia('screen and (max-width: 600px)');
	
	//!Enable CSS transitions
	$("html").removeClass("preload");
	

	if(mobileview.matches)
	{
		//Mobile only
	}
	else
	{
	}
		
	//!Mobile Nav
	var mobilenav = $('nav#mobile_nav');
	var body = $('body');
	var menubutton = $('.bars');
	var content = $("#container");
	var subnavs = mobilenav.find("ul > li > ul");
	
	subnavs.hide();
	
	function closeMenu() {
		body.removeClass("open");
		mobilenav.removeClass("open");
	}
	
	function toggleMenu() {
		body.toggleClass("open");
		mobilenav.toggleClass("open");
	}
	
	menubutton.on('click', function(e) { 
		toggleMenu();
		e.stopPropagation();
		e.preventDefault();
	});
	content.on('click', function(e) {
		if(body.hasClass("open"))
		{
			closeMenu();
			e.preventDefault();
		}
	});
	mobilenav.find('*').on('click', function (e) {
		if (e.target.nodeName === 'A' || e.target.nodeName === 'LI')
		{
			var subnav = $(e.target.parentElement).find("ul");
			
			if(subnav.length)
			{
				subnavs.slideUp();
				subnav.slideToggle();
				e.preventDefault();
			}
			else
			{
				closeMenu();
			}
		}
	});	

	//!Smooth scrolling for anchor links
	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') || location.hostname === this.hostname) 
	    {
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             $('html,body').animate({
	                 scrollTop: target.offset().top
	            }, 1000);
	            return false;
	        }
	    }
	});	
	
	
});