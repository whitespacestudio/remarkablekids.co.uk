<?php
/**
 * Holding page
 *
 * @author Steve Elford
 */
$slug = 'home';
require_once("includes/functions.php");
$pageTitle = SITE_NAME;
include(SRV_ROOT."includes/meta.php");
?>
</head>
<body>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-92534072-1', 'auto');
	  ga('send', 'pageview');
	
	</script>	
	<div id="wrapper">
		<header>
			<div class="container">
				<img id="logo" src="images/logo.png" alt="Remarkable Kids"/>
			</div>
		</header>
		
		<main>
			<div class="container">
				<p class="blue" style="font-size: 18px"><strong>Your child can learn to swim<br>
				Your child can learn to dance<br>
				Your child can learn to play sports<br>
				and<br>
				Your child can learn to become more confident</strong></p>
				
				<p class="orange" style="font-size: 14px">We’re running fun, friendly and safe, confidence building courses for kids between <br>
				5-11 years old and we’re coming to your area next month.</p>
				
				
				<p class="cyan" style="font-size: 16px">Find out what we do, how we do it and when and where we’re doing it by sending us an email at:</p>
				
				<a href="mailto:hi@remarkablekids.co.uk" class="button">hi@remarkablekids.co.uk</a>
				
				<img id="coming-soon" src="images/coming-soon.svg" alt="Coming soon" />
			</div>
		</main>
		<div class="push"></div>
		<div id="social"><p><span>Follow us:</span> <a href="https://www.facebook.com/Remarkable-Kids-742369995912961/" target="_blank"><img src="images/facebook-with-circle.svg"></a> <a href="https://twitter.com/Remarkable_Kids" target="_blank"><img src="images/twitter-with-circle.svg"></a></p></div>
	</div>	
	<footer>
		<img id="kids" src="images/kids.jpg" alt="kids" />
	</footer>

</body>