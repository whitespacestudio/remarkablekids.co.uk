<?php
/**
 * The page footer.
 * Included on every page. Includes the footer element and closes the wrapper elements.
 *
 * @author Steve Elford
 */
?>
					<footer>
						
						<div class="container">
																					
							<div class="footer_text">
								<p>&copy; Copyright <?=date('Y')?>.</p>
								<p class="credit"><a href="http://www.nexusdp.co.uk/" target="_blank">Site by Nexus <img src="<?=SITE_URL?>images/nexus_icon.svg" width="16" height="16"/></a></div>

							</div>
														
						</div>
													
					</footer>
				
				</div> <!-- #blur -->
			</div> <!-- #container -->
		</div> <!-- #wrap -->

	</body>
</html>