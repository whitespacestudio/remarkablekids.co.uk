<?php
/**
 * The page `<head>` section.
 * Included on every page. Opens the HTML document and includes all the necessary meta data.
 * The closing head tag is omitted to allow it to be extended on individual pages.
 *
 * @author Steve Elford 
 */
?>

<!DOCTYPE html>
<html lang="en" class="no-js preload">
	<head>
		<meta charset="utf-8" />
		
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
		<meta name="robots" content="NOFOLLOW,NOINDEX">
	
		<!-- Icons -->
		<link rel="shortcut icon" href="<?=SITE_URL?>images/favicon.ico?v=1">
		<link rel="apple-touch-icon" href="<?=SITE_URL?>images/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_URL?>images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_URL?>images/apple-touch-icon-114x114.png">
		
		<title><?php echo $pageTitle; ?></title>
		
		<script src="<?=SITE_URL?>js/modernizr.js"></script>
		
		<!--!CSS-->
		<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/e51cd027-6e4f-419a-b7d9-16ca1ecc44e4.css"/>
		<link rel="stylesheet" type="text/css" media="all" href="<?=SITE_URL?>css/styles.css?v=20170224"/>
		
		<!--!JS-->
		<script>
			var site_url = "<?=SITE_URL?>";
		</script>
		<script src="<?=SITE_URL?>js/jquery-3.0.0.min.js"></script>
		<script src="<?=SITE_URL?>js/global.js"></script>
