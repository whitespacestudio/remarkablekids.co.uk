<?php
/**
 * Global PHP functions and variable declarations.
 * TO be included on every page
 *
 * @author Steve Elford
 */

//SSL
$ssl_enabled = true;
$protocol = "http://";
if($ssl_enabled == true)
{
	$protocol = "https://";
}
function isSecure() {
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443;
}
if(isset($_SERVER['REQUEST_METHOD']))
{
	//SSL
	if($ssl_enabled)
	{
		if (!isSecure()) { 
		    $url = $protocol. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 
		    header("Location: $url"); 
		    exit; 	

		}
	}
	else
	{
		if (isSecure()) {
		    $url = $protocol. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 
		    header("Location: $url"); 
		    exit; 
		}
		
	}
}

/* Site name */
$site_name = "Remarkable Kids";
/* URL of the site homepage. Must end with a forward-slash */
$site_url = $protocol.getenv('HTTP_HOST')."/";
/* Full server path to the site root. Must end with a forward-slash */
$srv_root = $_SERVER["DOCUMENT_ROOT"]."/";


define('SITE_NAME', $site_name);
define('SITE_URL', $site_url);
define('SRV_ROOT', $srv_root);

$pageTitle = isset($pageTitle) ? $pageTitle : SITE_NAME;
if(!isset($slug)) $slug = '';
?>